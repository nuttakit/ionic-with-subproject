import { NgModule } from '@angular/core';
import { SandComponent } from './sand.component';
import { TargetComponent } from './target/target.component';
import { CalendarComponent } from './calendar/calendar.component';

@NgModule({
  declarations: [SandComponent, TargetComponent, CalendarComponent],
  imports: [
  ],
  exports: [SandComponent, TargetComponent, CalendarComponent]
})
export class SandModule { }
