import { TestBed } from '@angular/core/testing';

import { SandService } from './sand.service';

describe('SandService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SandService = TestBed.get(SandService);
    expect(service).toBeTruthy();
  });
});
