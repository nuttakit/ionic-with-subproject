import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';

import { SandModule } from 'sand';

import { TargetPageComponent } from './target-page/target-page.component';

@NgModule({
  declarations: [TargetPageComponent],
  imports: [
    CommonModule,
    IonicModule,
    SandModule,
    RouterModule.forChild([
      {
        path: '',
        component: TargetPageComponent
      }
    ])
  ]
})
export class TargetModule { }
