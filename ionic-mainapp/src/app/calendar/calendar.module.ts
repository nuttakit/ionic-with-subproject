import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';

import { SandModule } from 'sand';

import { CalendarPageComponent } from './calendar-page/calendar-page.component';

@NgModule({
  declarations: [CalendarPageComponent],
  imports: [
    CommonModule,
    IonicModule,
    SandModule,
    RouterModule.forChild([
      {
        path: '',
        component: CalendarPageComponent
      }
    ])
  ]
})
export class CalendarModule { }
